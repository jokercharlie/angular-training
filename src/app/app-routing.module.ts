import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { TestComponent } from './test/test.component';
import { UserComponent } from './user/user.component';

const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
    },{
        path: 'usuarios',
        component: UserComponent,
    },{
        path: 'test',
        component: TestComponent,
    },{
        path:'**',
        component: TestComponent
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
