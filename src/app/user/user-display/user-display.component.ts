import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-user-display',
  templateUrl: './user-display.component.html',
  styleUrls: ['./user-display.component.css']
})
export class UserDisplayComponent implements OnInit {
    @Input() users : User[];
    @Output() deleteUser = new EventEmitter<User>();
    @Output() editUser = new EventEmitter<User>();
    public isDeleting = false;

  constructor(private userService: UserService) { }

  ngOnInit() {
  }

  delete(user: User){
      
      this.deleteUser.emit(user);
  }

  edit(user: User){
      this.editUser.emit(user);
  }

}
