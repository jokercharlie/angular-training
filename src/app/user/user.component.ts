import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(private userService:UserService) { }
  public users: User[];

  ngOnInit() {
      this.getUsers();
      console.log(this.users);
  }

  public getUsers(){
      this.users = this.userService.getUsers();
  }

  public deleteUser(user: User){
      console.log(user);
  }

  public editUser(user:User){
      console.log("Editar");
      console.log(user);
  }

}
