import { Injectable } from '@angular/core';
import { User } from '../models/user';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor() { }

  returnUser: User[];

  Users = [
        {
         id : 1,
         name: 'Pablo',
         lastName: 'Monge',
         age: 31,
         birthDate: new Date('1989-04-02'),
         email: 'pabmon@gmail.com'
        },
        {
         id : 2,
         name: 'Pedro',
         lastName: 'Monge',
         age: 34,
         birthDate: new Date('1943-04-02'),
         email: 'pebmon@gmail.com'
        }
  ];

  public getUsers(){
      this.returnUser = this.Users;
      return this.returnUser;
  }

  public addUser(){

  }
  public removeUser(){

  }
  public editUser(){

  }
}
