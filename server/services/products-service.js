'use strict';
var logger = require('winston');
var async = require('asyncawait/async');
var await = require('asyncawait/await');
var productsRepository = require(__base + 'server/infrastructure/resources').products;

function getProducts() {
    var result;
    logger.debug('selecting all products');
    try {
        result = await(productsRepository.getProducts());
        logger.debug('result length ' + result.length);
    } catch (error) {
        throw error;
    }

    return { products: result };
}

function getProduct(id) {
    var result;
    logger.debug('selecting a product');
    try {
        result = await(productsRepository.getProduct(id));
        logger.debug('result length ' + result.length);
    } catch (error) {
        throw error;
    }

    return { product: result };
}

function addProduct(data) {
    var result;
    logger.debug('add product');
    try {
        result = await(productsRepository.addProduct(data));
    } catch (error) {
        throw error;
    }

    return { message: result };
}

function updateProduct(data) {
    var result;
    logger.debug('update product');
    try {
        result = await(productsRepository.updateProduct(data));
    } catch (error) {
        throw error;
    }

    return { message: result };
}

function deleteProduct(id) {
    var result;
    logger.debug('delete product');
    try {
        result = await(productsRepository.deleteProduct(id));
    } catch (error) {
        throw error;
    }

    return { message: result };
}



module.exports = {};
module.exports.getProducts = async(getProducts);
module.exports.getProduct = async(getProduct);
module.exports.addProduct = async(addProduct);
module.exports.updateProduct = async(updateProduct);
module.exports.deleteProduct = async(deleteProduct);