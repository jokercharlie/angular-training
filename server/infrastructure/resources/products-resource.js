var logger = require('winston');
var knex = require(__base + 'server/configuration').knex.knex;
var mysql = require('mysql');
var fs = require('fs');
var Promise = require('bluebird');

function getProducts(cb) {
    knex.select('*').from('products')
        .then(function (results) {
            return cb(null, results);
        })
        .catch(function (reason) {
            logger.error(reason);
            return cb(reason.message, null);
        });
}

function getProduct(id,cb) {
    knex.select('*').from('products')
        .where('idproduct','=',id)
            .then(function (results) {
                return cb(null, results);
            })
            .catch(function (reason) {
                logger.error(reason);
                return cb(reason.message, null);
            });
}

function addProduct(data,cb) {
    logger.info(data);
    knex('products').insert({
            name:data.name,
            measureunit:data.measureunit,
            category: data.category,
            cost: data.cost,
            price:data.price}).then(function(result){
                return cb(null,'Success!');
            })
            .catch(function (reason) {
                logger.error(reason);
                return cb(reason.message, null);
            });
    
}

function updateProduct(data,cb) {
    knex('products')
        .where('idproduct','=',data.idproduct)
        .update({name:data.name,
            measureunit:data.measureunit,
            category: data.category,
            cost: data.cost,
            price:data.price}).then(function(result){
                return cb(null,'Success!');
            })
            .catch(function (reason) {
                logger.error(reason);
                return cb(reason.message, null);
            });
    
}

function deleteProduct(id,cb) {
    logger.debug(id);
    knex('products')
        .where('idproduct','=',id)
        .del().then(function(result){
            return cb(null,'Success!');
        })
        .catch(function (reason) {
                logger.error(reason);
                return cb(reason.message, null);
            });
}

module.exports = {
    getProducts: Promise.promisify(getProducts),
    getProduct: Promise.promisify(getProduct),
    addProduct: Promise.promisify(addProduct),
    updateProduct: Promise.promisify(updateProduct),
    deleteProduct: Promise.promisify(deleteProduct),
};