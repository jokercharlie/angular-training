'use strict';
var logger = require('winston');
var await = require('asyncawait/await');
var async = require('asyncawait/async');
var handlers = require(__base + 'server/routes/router-handlers');
var service = require(__base + 'server/services');
var routes = require('express').Router();


function getProducts(request, response) {
    logger.debug('get products info');
    var result;
    try {
        logger.debug('retrieving result products ');
        result = await (service.productsService.getProducts());
        return handlers.successResponseHandler(response, result);
    } catch (error) {
        return handlers.errorResponseHandler(response, error);
    }
}

function getProduct(request, response) {
    logger.debug('get products info');
    var result;
    try {
        logger.debug('retrieving result products ');
        result = await (service.productsService.getProduct(request.params.productId));
        return handlers.successResponseHandler(response, result);
    } catch (error) {
        return handlers.errorResponseHandler(response, error);
    }
}

function addProduct(request, response) {
    logger.debug('add product info');
    var result;
    try { 
        console.log(request.body);
        result = await (service.productsService.addProduct(request.body));
        return handlers.successResponseHandler(response, result);
    } catch (error) {
        return handlers.errorResponseHandler(response, error);
    }
}

function updateProduct(request, response) {
    logger.debug('update product info');
    var result;
    try {
        logger.info(request.body);
        result = await (service.productsService.updateProduct(request.body));
        return handlers.successResponseHandler(response, result);
    } catch (error) {
        return handlers.errorResponseHandler(response, error);
    }
}

function deleteProduct(request, response) {
    logger.debug('delete product info');
    var result;
    try {
        logger.info(request.body);
        result = await (service.productsService.deleteProduct(request.params.productId));
        return handlers.successResponseHandler(response, result);
    } catch (error) {
        return handlers.errorResponseHandler(response, error);
    }
}


routes.get('/', async(getProducts));
routes.get('/:productId', async(getProduct));
routes.post('/', async(addProduct));
routes.put('/', async(updateProduct));
routes.delete('/:productId', async(deleteProduct));


module.exports = routes;